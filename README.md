## TDForms

TDForms is an iOS library by [The Distance](http://thedistance.co.uk) for creating forms.

## How to make a form

Include TDFormViewController.h, then just subclass TDFormViewController (note – if the view controller is in a storyboard then drag a Table View Controller rather than a normal View Controller onto the storyboard).

In the subclass, as a minimum all you have to do is configure the form in viewDidLoad, and deal with submission in formSubmitted

```objective-c
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //make some cells using various TDCell subclass types

    TDLabelCell *labelCell = [TDLabelCell labelCellWithTitle:@"Label" subtitle:@"Subtitle" value:@"Right hand side" key:nil];

    TDTextFieldCell *textFieldCell = [TDTextFieldCell textFieldCellWithTitle:@"Text" placeholder:@"Enter some text" key:@"text"];

    TDSubmitCell *submitCell = [TDSubmitCell submitCellWithTitle:@"Submit"];

    //add all the cells to the cells array (array of arrays - sections->cells)

    self.cells = @[@[labelCell,textFieldCell],
                   @[submitCell]];

    //always refresh after adding new cells

    [self refresh];
}

- (void)formSubmitted:(NSDictionary*)form {
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Success"
                                                      message:@""
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
    //form dictionary contains values for all the cells that were given keys
    NSLog(@"Results:\n%@",form);
}
```

We separate the process of making the cells and setting their values. It's nearly always best to create the cells ONCE in viewDidLoad. If you need to set values on viewWillAppear or viewDidAppear, then you can just call updateValues at this point (or whenever you need an update), with a dictionary that has keys for whatever cells you want to change, and corresponding values (different cell types require variously NSString, NSNumber, NSDate, UIImage...)

```objective-c
[self updateValues:@{   @"text2":@"some text",
                        @"text3":@"more text"     }];
```

## Validation

To do automatic validation of forms, make TDValidation objects, and add them to the appropriate cells

```objective-c
TDValidation *nonnullValidation = [TDValidation validationWithRegex:@".+" message:@"You left the non-null field null"];
myCell.validations = @[nonnullValidation]; //it's an array, you can have as many validations as you want
```

```objective-c
//regex allowing only all caps postcodes
TDValidation *postcodeValidation = [TDValidation validationWithRegex:@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX‌​]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY]))))\\s?[0-9][A-Z-[C‌​IKMOV]]{2})" message:@"You entered an invalid postcode"];
postcodeCell.validations = @[postcodeValidation];
```

In your form's view controller viewDidLoad, set the multipleErrorMessage property for when more than one validation fails. The multipleErrorMessage will show, followed by each of the validation failure messages.

```objective-c
self.multipleErrorMessage = @"Oops there were some problems";
```

## Dynamic forms

You can use the cellValueChanged method in your form's view controller if you need to do things prior to the submit button being pressed. For example, you can insert or delete rows in the form, by simply updating the cells array property.


Then you can just add or remove cells whenever you want:

```objective-c
- (void)cellValueChanged:(TDCell*)cell {
    
    if ([cell.key isEqualToString:@"mycell"]) {
        
        //let's add a label cell whenever mycell has a value
        //all you have to do is update the cells array,
        //and if need be call insertRowsAtIndexPaths/deleteRowsAtIndexPaths for animations
		//but here we will just refresh, not bothered about animation
        
        if ([cell.value isEqualToString:@""]) {

			self.cells = @[@[],self.cells[1]]; //make section 0 empty, but keep section 1 the same as it was before
        }
        else {

            TDLabelCell *labelCell = [TDLabelCell labelCellWithTitle:@"Label" subtitle:@"Subtitle" value:cell.value key:@"labelCell"];
            
			self.cells = @[@[labelCell],self.cells[1]]; //put lnew label in section 0, but keep section 1 the same as it was before
        }
    }
}

```

If you set up some of the sections (in self.cells) as mutable arrays, then you can also just do things like:

```objective-c
[self.cells[0] insertObject:newCell atIndex:0];
```

you might need to insert and delete rows at any point you want, not just on cellValueChanged.

## TDLabelCell

TDLabelCell changes its layout depending on the data present. Use it for title-subtitle-value cells, or leave any of those blank for different layouts like value filling the whole cell, just title etc etc.

To save changing the font every time you want to use it as a header type of label, use the UIAppearance proxy property titleFontWhenNoSubtitleOrValue

```objective-c
//label title 15pt normally, 20pt when using as a header
[[TDLabelCell appearance] setTitleFont:[UIFont systemFontOfSize:15]];
[[TDLabelCell appearance] setTitleFontWhenNoSubtitleOrValue:[UIFont systemFontOfSize:20]];    
```

## TDParentFormCell

TDParentFormCell is a special type of TDCell, that pushes a new view controller onto the navigation stack (usually another TDFormViewController, but it can be any view controller).

The new view controller must conform to the TDChildViewController protocol, which simply means it has a property called parent. 

```objective-c
@interface SubFormViewController : TDFormViewController <TDChildViewController>

@property (nonatomic, strong) TDParentFormCell *parent;

@end
```

The parent will be automatically set to the TDParentFormCell, and it is the TDChildViewController's responsibility to set its self.parent.value, for whatever the value of the TDParentFormCell might need to be. For example...

```objective-c
- (void)formSubmitted:(NSDictionary*)form {
    
    self.parent.value = form;
    
    [self.navigationController popViewControllerAnimated:YES];
}
```

## Appearance

Set global appearance properties in your appDelegate didLaunch

```objective-c
[[TDCell appearance] setHeight:70];
[[TDCell appearance] setCellColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
[[TDCell appearance] setTextColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
[[TDCell appearance] setFont:[UIFont systemFontOfSize:14]];

UIImage *buttonBackgroundImage = [[UIImage imageNamed:@"ButtonBackground"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 3, 0)];
[[TDSubmitCell appearance] setBackgroundImage:buttonBackgroundImage];

//etc
```

Check the headers for TDCell and its subclasses and widgets to see which UI_APPEARANCE_SELECTOR properties are available. Bearing in mind that any cell type subclass also inherits the ones in TDCell.

Each of these can also be set on a cell by cell basis to override any global settings

```objective-c
TDTextFieldCell *cell = [TDTextFieldCell textFieldCellWithTitle:@"A Cell" placeholder:@"Enter some text" key:@"cell"];

//customize it

cell.height = 100;
cell.leftPadding = 30;
cell.cellColor = [UIColor darkGrayColor];
cell.textColor = [UIColor whiteColor];
cell.font = [UIFont systemFontOfSize:10];

cell.textFieldWidthAsProportion = 0.7;

cell.bottomSeparatorHeight = 5;
cell.bottomSeparatorColor = [UIColor whiteColor];
cell.bottomSeparatorWidthAsProportion = 1.0;

//we can also customize the textField, because it is exposed by TDTextFieldCell

cell.textField.textColor = [UIColor redColor];
cell.textField.font = [UIFont systemFontOfSize:8];
[cell.textField setAutocapitalizationType:UITextAutocapitalizationTypeAllCharacters];
[cell.textField setAutocorrectionType:UITextAutocorrectionTypeNo];
[cell.textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
```

## Subclassing TDCell

If you need a custom cell that is very specific to one project, just subclass TDCell and add it to that project, don't add it to the library.

But if it's a generic type of cell and will be useful in other projects then add it to the library, making sure that it offers all the properties etc to make it reusable in all different situations.

Copy the coding style in TDLabelCell and TDTextFieldCell – i.e. use NSLayoutConstraints for everything, use UI_APPEARANCE_SELECTOR wherever possible. Also, expose individual elements like labels and text fields, so they can be customized by the user on an individual basis if they want.

Provide a convenience constructor with at least the key as an argument, and use the uuid method for a reuse id,

```objective-c
+ (TDTextFieldCell*)textFieldCellWithTitle:(NSString*)title placeholder:(NSString*)placeholder key:(NSString*)key
{
    TDTextFieldCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.textField.placeholder = placeholder;
    cell.key = key;
    
    return cell;
}
```

Init stuff in initWithStyle, setting uiappearance defaults to the ivars.

```objective-c
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        _textFieldWidthAsProportion = 0.5;
        _titleToTextFieldPadding = 16;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
        self.textField = [[TDTextField alloc] init];
        self.textField.delegate = self;
        self.textField.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
        self.textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.textField];
        
    }
    return self;
}
```

Lay stuff out in layoutSubviews (UIAppearance properties are set properly by this point). Note the self.textField.translatesAutoresizingMaskIntoConstraints = NO in init, and layout constraints relative to self.contentView. All constraints are removed in the super class's layoutSubviews, so it's safe to add them each time from scratch.

```objective-c
- (void)layoutSubviews {
    
    [super layoutSubviews];

    [self.contentView addConstraint:[NSLayoutConstraint
                                     constraintWithItem:self.textField
                                     attribute:NSLayoutAttributeWidth
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:self.contentView
                                     attribute:NSLayoutAttributeWidth
                                     multiplier:self.textFieldWidthAsProportion
                                     constant:0]];
    ...
```

Provide a (id)value method that gives the current value, and a setter that updates the UI.

```objective-c
- (id)value {
    
    return self.textField.text;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.textField.text = value;
    }
}
```

Provide select and done methods for showing/dismissing any keyboards, pickers etc

```objective-c
- (void)select {
    [self.textField becomeFirstResponder];
    [super done];
}

- (void)done {
    [self.textField resignFirstResponder];
    [super done];
}
```

Call cellValueChanged on the formViewController whenever your cell value changes, e.g.

```objective-c
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.formViewController cellValueChanged:self];
}
```

The accessory view for things like keyboards and pickers can be created like this

```objective-c
self.textField.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
```

or otherwise be sure to manually call the formViewController next and prev and done methods when needed.

## Library TODOs

- validation: at the moment only regex validation of NSStrings is there. But the type of the "value" property in each cell is just "id", so new validation methods for any data type can be added to the library as and when we need them. For example, one that uses value as a NSDate, and only allows dates within a certain period, etc. (although perhaps date is a bad example, because min and max dates can be set on the date picker itself)

New cell types needed:

- search? (as seen in clever accounts designs?)
- location? (with current position button etc?)

Nice to have:

- Support for JSON definition of form
- Binding to object so it will auto generate form and save back to object (may need hooks to save into Core Data) - may need a support definition file for addition validation, or perhaps there is option to add definition in the class (like .net attributes)

Bugs:

- initial testing in ios7 shows some differences in the way text fields update. May be only an ios7 beta bug?
