//
//  ViewController.m
//  TDForms
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    TDTextFieldCell *textField = [TDTextFieldCell textFieldCellWithTitle:@"Text field" placeholder:@"Enter some text" key:@"text"];
    textField.validations = @[[TDValidation nonNullValidationWithmessage:@"Fill in the text field"]];
    
    TDTextFieldCell *textField2 = [TDTextFieldCell textFieldCellWithTitle:@"Text field" placeholder:@"Enter some text" key:@"text2"];
    TDTextFieldCell *textField3 = [TDTextFieldCell textFieldCellWithTitle:@"Text field" placeholder:@"Enter some text" key:@"text3"];
    TDTextFieldCell *textField4 = [TDTextFieldCell textFieldCellWithTitle:@"Text field" placeholder:@"Enter some text" key:@"text4"];
    TDTextFieldCell *textField5 = [TDTextFieldCell textFieldCellWithTitle:@"Text field" placeholder:@"Enter some text" key:@"text5"];
    TDTextFieldCell *textField6 = [TDTextFieldCell textFieldCellWithTitle:@"Text field" placeholder:@"Enter some text" key:@"text6"];
    
    TDPickerCell *picker = [TDPickerCell pickerCellWithTitle:@"Picker" values:@[@"1",@"2",@"3"] key:@"picker"];
    
    TDDateCell *date = [TDDateCell dateCellWithTitle:@"Date" value:nil key:@"date"];
    date.datePicker.minimumDate = [NSDate date];
    
    TDListSelectCell *listSelect = [TDListSelectCell listSelectCellWithTitle:@"List" values:@[@"1",@"2",@"3"] multipleSelect:NO key:@"list"];
    
    //multiple select cells require a selection image to be set
    //TDListSelectCell *multipleListSelect = [TDListSelectCell listSelectCellWithTitle:@"Multilist" values:@[@"1",@"2",@"3"] multipleSelect:YES key:@"multilist"];
    
    TDImageSelectCell *imageSelect = [TDImageSelectCell imageSelectCellWithTitle:@"Image" image:nil key:@"image"];

    TDSubmitCell *submitCell = [TDSubmitCell submitCellWithTitle:@"Submit"];

    self.cells = @[ @[textField, textField2,textField3,textField4,textField5,textField6, picker, date, listSelect, imageSelect], @[submitCell] ];

    [self refresh];
}

- (IBAction)setSomeCellValues:(id)sender {
    
    [self updateValues:@{   @"text2":@"text 2 auto",
                            @"text3":@"text 3 auto"     }];
}

- (void)cellValueChanged:(TDCell*)cell {

}

- (void)formSubmitted:(NSDictionary*)form {
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Success"
                                                      message:@""
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
    //form dictionary contains values for all the cells that were given keys
    NSLog(@"Results:\n%@",form);
}

@end
