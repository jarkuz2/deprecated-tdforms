//
//  TDSwitchCell.h
//  CleverAccounts
//
//  Created by pw on 25/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"

@interface TDSwitchCell : TDCell

+ (TDSwitchCell*)switchCellWithTitle:(NSString*)title value:(NSNumber*)value key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;

@property (strong, nonatomic) UISwitch *switchControl;
@property (strong, nonatomic) UIButton *buttonControl;

@property (strong, nonatomic) UIImage *onImage UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *offImage UI_APPEARANCE_SELECTOR;

@end
