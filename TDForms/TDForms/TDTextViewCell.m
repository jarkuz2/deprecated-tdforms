//
//  TDTextViewCell.m
//  CleverAccounts
//
//  Created by pw on 24/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDTextViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "TDTextView.h"
#import "TDInputAccessoryView.h"
#import "TDFormViewController.h"

@interface TDTextViewCell() <UITextViewDelegate>

@end

@implementation TDTextViewCell

+ (TDTextViewCell*)textViewCellWithTitle:(NSString*)title value:(NSString *)value key:(NSString*)key
{
    TDTextViewCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.key = key;
    cell.textView.text = value;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        _titleToTextViewPadding = 8;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
        self.textView = [[TDTextView alloc] init];
        self.textView.delegate = self;
        self.textView.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
        self.textView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.textView];
        
        
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    
    [self.titleLabel sizeToFit];
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    [self removeConstraintsForView:self.textView onView:self.contentView];
    
    if (self.titleLabel != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
    }
    
    
    if (self.textView != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textView
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.titleLabel
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:self.titleToTextViewPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textView
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textView
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.textView
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
    }
    
    [super updateConstraints];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [self.formViewController cellValueChanged:self];
}

- (id)value {
    
    return self.textView.text;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.textView.text = value;
    }
}

- (BOOL)select {

    NSIndexPath *indexPath = [self.formViewController.tableView indexPathForRowAtPoint:self.center];

    [UIView animateWithDuration:0.3
        animations: ^{
            
            [self.formViewController.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
            
        }completion: ^(BOOL finished){
            
            [self performSelector:@selector(doFirstResponder) withObject:nil afterDelay:0.1];
        }
     ];
    
    return YES;
}

- (void)doFirstResponder {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.formViewController.scrolling = NO;
        [self.textView becomeFirstResponder];
    });
}

- (void)done {
    
    [self.textView resignFirstResponder];
}

@end
