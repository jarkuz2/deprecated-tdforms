//
//  TDImageSelectCell.h
//  CleverAccounts
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"
#import <UIKit/UIKit.h>

@interface TDImageSelectCell : TDCell

+ (TDImageSelectCell*)imageSelectCellWithTitle:(NSString*)title image:(UIImage*)image key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIImageView *thumbnailView;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *disclosureIndicatorView;


@property (strong, nonatomic) UIButton *removeImageButton;
@property (strong, nonatomic) UIButton *addImageButton;

@property (strong, nonatomic) UIColor *titleColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *titleFont UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *backButtonImage UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *editButtonImage UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *addImageButtonImage UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *removeImageButtonImage UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *thumbnailBackgroundImage UI_APPEARANCE_SELECTOR;

@property (assign) CGFloat padding UI_APPEARANCE_SELECTOR;

@property (assign) CGFloat thumbnailTopPadding UI_APPEARANCE_SELECTOR;
@property (assign) CGFloat thumbnailBottomPadding UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) NSString *removeImageTitle UI_APPEARANCE_SELECTOR;

@end
