//
//  TDTextField.m
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDTextField.h"
#import "TDCell.h"
#import "TDFormViewController.h"

@interface TDTextField()

@property (nonatomic, strong) UIView *topBorder;
@property (nonatomic, strong) UIView *bottomBorder;
@property (nonatomic, strong) UIView *leftBorder;
@property (nonatomic, strong) UIView *rightBorder;

@end

@implementation TDTextField


- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if (![[TDTextField appearance] font]) {
        if (![[TDCell appearance] font]) {
            self.font = [UIFont systemFontOfSize:14];
        }
        else {
            self.font = [[TDCell appearance] font];
        }
    }
    else {
        self.font = [[TDTextField appearance] font];
    }
    
    if (self.background == nil) self.background = [[TDTextField appearance] backgroundImage];
}


- (CGRect)textRectForBounds:(CGRect)bounds
{
    return [self textAndEditingRectsForBounds:bounds];
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textAndEditingRectsForBounds:bounds];
}



- (CGRect)textAndEditingRectsForBounds:(CGRect)bounds
{
    CGRect rect = [super textRectForBounds:bounds];
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue] > 6.9f) {
        
        rect.origin.x += self.textPadding;
        rect.size.width -= self.textPadding*2.0;
    }
    else {

     //align vertically central
     CGSize size = [@"Text" sizeWithFont:self.font];
     rect.size.height = size.height;
     rect.origin.y = (self.frame.size.height / 2.0) - (size.height / 2.0);
     
     //give some padding left and right
     rect.origin.x += self.textPadding;
     rect.size.width -= self.textPadding*2.0;
    }
    
    
    
    return rect;
}



- (BOOL)resignFirstResponder {
    
    if ([self.superview.superview.superview isKindOfClass:[UITableView class]]) {
        if ([((UITableView*)self.superview.superview.superview).dataSource isKindOfClass:[TDFormViewController class]]) {
            
            TDFormViewController *formVC = ((TDFormViewController*)((UITableView*)self.superview.superview.superview).dataSource);
            if (formVC.scrolling) {
                return NO;
            }
        }
    }
    return [super resignFirstResponder];
}

@end