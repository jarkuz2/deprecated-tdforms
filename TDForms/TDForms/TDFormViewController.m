//
//  TDFormViewController.m
//  CleverAccounts
//
//  Created by pw on 24/05/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDFormViewController.h"

@interface TDFormViewController ()

@end

@implementation TDFormViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

- (void)refresh {
    
    int sectionIndex = 0;
    for (NSArray *section in self.cells) {
        
        int rowIndex = 0;
        for (TDCell *cell in section) {
            
            cell.formViewController = self;

            rowIndex++;
        }
        sectionIndex++;
    }
    
    [self.tableView reloadData];
}

- (void)submit {
    
    [self dismissAllKeyboards];
    
    NSMutableArray *errors = [NSMutableArray array];
    
    NSIndexPath *firstFailedCellIndexPath = nil;
    
    int sectionIndex = 0;
    for (NSArray *section in self.cells) {
        int rowIndex = 0;
        for (TDCell *cell in section) {
            for (TDValidation *validation in cell.validations) {
                              
                //reset all fields to normal background
                //and text colour
                for (id vv in cell.contentView.subviews) {
                    
                    if ([vv isKindOfClass:[TDTextField class]]) {
                        
                        TDTextField*tff = (TDTextField*)vv;
                        
                        UIImage *textFieldBackgroundImage = [[UIImage imageNamed:@"TextFieldBackground"]
                                                             resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 3, 0)];
                        tff.background = textFieldBackgroundImage;
                        
                        if (tff.text !=nil ) {
                            
                            tff.textColor = [UIColor blackColor];
                        }
                    }
                    
                }

                
                if (![validation validate:cell.value]) {
                    
                    //capture the title of the field and the message
                    [errors addObject:[NSString stringWithFormat:@"%@: %@",cell.title,validation.message]];
                    
                    if (firstFailedCellIndexPath == nil) firstFailedCellIndexPath = [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
                    
                    //iterate through views and set the cell background to red and the text if required
                    for (id vv in cell.contentView.subviews) {
                        
                        if ([vv isKindOfClass:[TDTextField class]]) {
                            
                            TDTextField*tff = (TDTextField*)vv;
                            
                            UIImage *textFieldBackgroundImage = [[UIImage imageNamed:@"ButtonBackgroundValidation"]
                                                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 3, 0)];
                         tff.background = textFieldBackgroundImage;
                            
                            if (tff.text !=nil ) {
                                
                               // tff.textColor = [UIColor redColor];
                            }
                        }
                        
                    }
                   
                }
            }
            rowIndex++;
        }
        sectionIndex++;
    }
    
    if ([errors count] == 0) {
        
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        
        for (NSArray *section in self.cells) {
            for (TDCell *cell in section) {
                
                if (cell.key != nil) {
                    
                    if (cell.value == nil) [dictionary setObject:@"" forKey:cell.key];
                    else [dictionary setObject:cell.value forKey:cell.key];
                }
            }
        }
        
        [self formSubmitted:dictionary];
    }
    else if ([errors count] == 1) {
        
        [self.tableView scrollToRowAtIndexPath:firstFailedCellIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:errors[0]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];

    }
    else {

        [self.tableView scrollToRowAtIndexPath:firstFailedCellIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
        
        NSMutableString *errorsConcatenated = [NSMutableString stringWithString:@"\n"];
        for (NSString *error in errors) {
            [errorsConcatenated appendString:error];
            if ([errors indexOfObject:error] != [errors count]-1) [errorsConcatenated appendString:@"\n\n"];
        }
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Form error's"]
                                                          message:errorsConcatenated
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }
}


-(void)updateValues:(NSDictionary*)values {

    for (NSString *key in [values allKeys]) {
        
        TDCell *theCell;
        for (NSArray *section in self.cells) {
            for (TDCell *cell in section) {
                if ([cell.key isEqualToString:key]) {
                    theCell = cell;
                    break;
                }
            }
        }
        
        if (theCell != nil) {
            theCell.value = values[key];
        }
    }
}

- (void)cellValueChanged:(TDCell *)cell {
    //subclasses can optionally override this to do something when a cell value changes!
}

- (void)formSubmitted:(NSDictionary*)form {
    //subclasses should override this to do something when the form is submitted!!
}

- (void)cellDeletedAtIndexPath:(NSIndexPath*)indexPath {
    //subclasses override if model needs to be updated on cell deletion
}

- (void)dismissAllKeyboards {
    
    [self.view endEditing:YES];
    
    for (NSArray *section in self.cells) {
        for (TDCell *cell in section) {
            if ([cell isKindOfClass:[TDPickerCell class]]) {
                [cell done];
            }
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.cells count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cells[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ((TDCell*)self.cells[indexPath.section][indexPath.row]).cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    TDCell *cellFromArray = (TDCell*)self.cells[indexPath.section][indexPath.row];
    
    TDCell *cell = [tableView dequeueReusableCellWithIdentifier:cellFromArray.reuseIdentifier];
    
    if (cell == nil) cell = cellFromArray;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section < [self.cells count]) {
        if (indexPath.row < [self.cells[indexPath.section] count]) {
            return ((TDCell*)self.cells[indexPath.section][indexPath.row]).editable;
        }
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray *mutableCells = [NSMutableArray arrayWithArray:self.cells];
        
        NSMutableArray *mutableSection = [NSMutableArray arrayWithArray:self.cells[indexPath.section]];
        
        [mutableSection removeObjectAtIndex:indexPath.row];
        
        [mutableCells replaceObjectAtIndex:indexPath.section withObject:mutableSection];
        
        self.cells = [NSArray arrayWithArray:mutableCells];
        
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self cellDeletedAtIndexPath:indexPath];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TDCell *cell = self.cells[indexPath.section][indexPath.row];
    [cell select];
}

#pragma mark - Input accessory view delegate

- (void)next:(TDCell*)fromCell
{
    int sectionIndex = 0;
    int cellIndex = 0;
    
    for (NSArray *section in self.cells) {
        
        int index = [section indexOfObject:fromCell];
        
        if (index != NSNotFound) {
            cellIndex = index + 1;
            break;
        }
        else {
            sectionIndex++;
        }
    }
    
    while (sectionIndex < [self.cells count]) {
                
        NSArray *section = self.cells[sectionIndex];

        while (cellIndex < [section count]) {
                    
            if ([(TDCell*)section[cellIndex] select]) {
                
                if ([fromCell isKindOfClass:[TDPickerCell class]]) [fromCell done];
                return;
            }

            cellIndex++;
        }
        
        sectionIndex++;
    }
    
    [fromCell done];
    
}



- (void)prev:(TDCell*)fromCell
{    
    int sectionIndex = 0;
    int cellIndex = 0;
    
    for (NSArray *section in self.cells) {
        
        int index = [section indexOfObject:fromCell];
        
        if (index != NSNotFound) {
            cellIndex = index - 1;
            break;
        }
        else {
            sectionIndex++;
        }
    }
    
    while (sectionIndex >= 0) {
        
        NSArray *section = self.cells[sectionIndex];
        
        while (cellIndex >= 0) {
            
            if ([(TDCell*)section[cellIndex] select]) {
                
                if ([fromCell isKindOfClass:[TDPickerCell class]]) [fromCell done];
                return;
            }
            
            cellIndex--;
        }
        
        sectionIndex--;
    }
    
    [fromCell done];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.scrolling = YES;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.scrolling = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.scrolling = NO;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    self.scrolling = NO;
}

@end
