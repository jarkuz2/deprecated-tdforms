//
//  TDButtonCell.m
//  CleverAccounts
//
//  Created by pw on 24/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDButtonCell.h"
#import "TDButton.h"

@implementation TDButtonCell

+ (TDButtonCell*)buttonCellWithLabelText:(NSString*)title titles:(NSArray*)titles images:(NSArray*)images selectorStrings:(NSArray*)selectors target:(id)target
{
    TDButtonCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.labelText = title;
    
    cell.titles = titles;
    cell.images = images;
    cell.targetForAllSelectors = target;
    cell.selectors = selectors;
    
    return cell;
}

+ (TDButtonCell*)buttonCellWithTitles:(NSArray*)titles images:(NSArray*)images selectorStrings:(NSArray*)selectors target:(id)target
{
    TDButtonCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titles = titles;
    cell.images = images;
    cell.targetForAllSelectors = target;
    cell.selectors = selectors;
    
    return cell;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _padding = 10;
        
        self.buttons = [NSMutableArray array];
        
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if (self.labelText && self.titleLabel == nil) {
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.text = self.labelText;
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.titleLabel];
        [self setNeedsUpdateConstraints];
    }
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    
    for (TDButton *button in self.buttons) [button removeFromSuperview];
    [self.buttons removeAllObjects];
    
    TDButton *previousButton = nil;
    int i = 0;
    for (NSString *title in self.titles) {
        
        UIImage *image = nil;
        if (i < [self.images count]) image = self.images[i];
        
        if (image == nil) image = self.backgroundImage;
        
        NSString *selector = self.selectors[i];
        
        TDButton *button;
        
        if (image == nil) {
            
            button = [TDButton buttonWithType:UIButtonTypeRoundedRect];
        }
        else {
            
            button = [TDButton buttonWithType:UIButtonTypeCustom];
            [button setImage:image forState:UIControlStateNormal];
        }
        
        [button setTitle:title forState:UIControlStateNormal];
        [button addTarget:self.targetForAllSelectors action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchUpInside];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:button];
        [self setNeedsUpdateConstraints];
        [self.buttons addObject:button];
        
        int maxWidth = 0;
        for (NSString *title in self.titles) {
            CGSize size = [title sizeWithFont:button.titleLabel.font];
            if (size.width > maxWidth) maxWidth = size.width;
        }
        
        NSLayoutConstraint *widthConstraint = [NSLayoutConstraint
                                               constraintWithItem:button
                                               attribute:NSLayoutAttributeWidth
                                               relatedBy:NSLayoutRelationEqual
                                               toItem:nil
                                               attribute:NSLayoutAttributeWidth
                                               multiplier:1.0
                                               constant:maxWidth+(self.padding*2)];
        
        if (self.buttonWidthConstraints == nil) {
            self.buttonWidthConstraints = [NSMutableArray arrayWithCapacity:self.buttons.count];
        }
        
        if (self.buttonWidthConstraints.count > i) {
            NSLayoutConstraint *constr = self.buttonWidthConstraints[i];
            constr.constant = widthConstraint.constant;
        } else {
            [button addConstraint:widthConstraint];
            [self.buttonWidthConstraints addObject:widthConstraint];
        }
        
        i++;
        previousButton = button;
    }
}

-(void)updateConstraints
{
    TDButton *previousButton = nil;
    int i = 0;
    for (TDButton *button in self.buttons) {
        
        [self removeConstraintsForView:button onView:self.contentView];
        
        if (self.titleLabel == nil) {
            
            if (previousButton == nil) {
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:button
                                                 attribute:NSLayoutAttributeLeft
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contentView
                                                 attribute:NSLayoutAttributeLeft
                                                 multiplier:1
                                                 constant:self.leftPadding]];
            }
            else {
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:button
                                                 attribute:NSLayoutAttributeLeft
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:previousButton
                                                 attribute:NSLayoutAttributeRight
                                                 multiplier:1
                                                 constant:self.padding]];
            }
            
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:button
                                             attribute:NSLayoutAttributeWidth
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeWidth
                                             multiplier:1.0/(float)[self.titles count]
                                             constant:(-self.padding * ((float)[self.titles count]-1.0) / (float)[self.titles count]) + ((-self.leftPadding-self.rightPadding) / (float)[self.titles count]) ]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:button
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1
                                             constant:self.topPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:button
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1
                                             constant:-self.bottomPadding]];
            
        }
        else {
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.titleLabel
                                             attribute:NSLayoutAttributeLeft
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeLeft
                                             multiplier:1
                                             constant:self.leftPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.titleLabel
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeRight
                                             multiplier:1
                                             constant:-self.rightPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.titleLabel
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1
                                             constant:self.topPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.titleLabel
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1
                                             constant:-self.bottomPadding]];
            
            
            if (previousButton == nil) {
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:button
                                                 attribute:NSLayoutAttributeRight
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contentView
                                                 attribute:NSLayoutAttributeRight
                                                 multiplier:1
                                                 constant:-self.rightPadding]];
            }
            else {
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:button
                                                 attribute:NSLayoutAttributeRight
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:previousButton
                                                 attribute:NSLayoutAttributeLeft
                                                 multiplier:1
                                                 constant:-self.padding]];
            }
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:button
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1
                                             constant:self.topPadding*2]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:button
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1
                                             constant:-self.bottomPadding*2]];
        }
        
        i++;
        previousButton = button;
    }
    
    [super updateConstraints];
}

@end
