//
//  TDImageSelectCell.m
//  CleverAccounts
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDImageSelectCell.h"
#import "TDFormViewController.h"


@interface TDImageSelectCell() < UIActionSheetDelegate>
@end

@implementation TDImageSelectCell

+ (TDImageSelectCell*)imageSelectCellWithTitle:(NSString*)title image:(UIImage*)image key:(NSString*)key
{
    TDImageSelectCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.image = image;
    cell.key = key;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
        self.thumbnailView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,10,10)];
        self.thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.thumbnailView];
        self.thumbnailView.backgroundColor = [UIColor clearColor];
		
           
    }
    return self;
}


- (void)setImage:(UIImage *)image {
    
    _image = image;
    
    float ratio = image.size.width / image.size.height;
    float height = self.cellHeight - self.topPadding - self.bottomPadding;
    float width = height * ratio;
    CGSize size = CGSizeMake(width, height);
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width,size.height)];
    UIImage *thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.thumbnailView.image = thumbnail;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.image = self.image; //ensures resizing is done after proper cell height is set etc
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    
    if (self.titleColor) self.titleLabel.textColor = self.titleColor;
    if (self.titleFont) self.titleLabel.font = self.titleFont;
    


	if (self.addImageButtonImage != nil && self.addImageButton == nil) {

		self.addImageButton = [TDButton buttonWithType:UIButtonTypeCustom];
		[self.addImageButton setImage:self.addImageButtonImage forState:UIControlStateNormal];
		
		[self.addImageButton setTitle:self.titleLabel.text forState:UIControlStateNormal];
        [self.addImageButton addTarget:self action:@selector(select) forControlEvents:UIControlEventTouchUpInside];
        self.addImageButton.translatesAutoresizingMaskIntoConstraints = NO;
		
		self.addImageButton.titleLabel.font = self.font;
		self.addImageButton.titleLabel.textColor = self.textColor;
		
		[self.contentView addSubview:self.addImageButton];
        [self setNeedsUpdateConstraints];
	}
	
	if (self.removeImageButtonImage != nil && self.removeImageButton == nil) {

		self.removeImageButton = [TDButton buttonWithType:UIButtonTypeCustom];
		[self.removeImageButton setImage:self.removeImageButtonImage forState:UIControlStateNormal];
		
		[self.removeImageButton setTitle:self.removeImageTitle forState:UIControlStateNormal];
        [self.removeImageButton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
        self.removeImageButton.translatesAutoresizingMaskIntoConstraints = NO;
		
		self.removeImageButton.titleLabel.font = self.font;
		self.removeImageButton.titleLabel.textColor = self.textColor;
		
		[self.contentView addSubview:self.removeImageButton];
        [self setNeedsUpdateConstraints];
	}
	
		
    
    if (self.cellSelectionColor != nil) {
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.contentView.bounds];
        self.selectedBackgroundView.backgroundColor = self.cellSelectionColor;
    }
	
	if ((self.image == nil && self.addImageButtonImage == nil) ||
		(self.image != nil && self.removeImageButtonImage == nil)) {
		
		[self layoutDisclosureStyle];
	}
	else {
		
		[self layoutButtonStyle];
	}
}

-(void)updateConstraints
{
    if ((self.image == nil && self.addImageButtonImage == nil) ||
        (self.image != nil && self.removeImageButtonImage == nil)) {
        
        [self updateConstraintsDisclosureStyle];
    }
    else {
        
        [self updateConstraintsButtonStyle];
    }
    
    
    [super updateConstraints];
}

- (void)layoutDisclosureStyle {

	
    if (self.disclosureIndicatorView == nil) {
        
        if (self.disclosureIndicator == nil) self.disclosureIndicator = [[TDCell appearance] disclosureIndicator];
        
        self.disclosureIndicatorView = [[UIImageView alloc] initWithImage:self.disclosureIndicator];
        self.disclosureIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.disclosureIndicatorView];
        [self setNeedsUpdateConstraints];
    }
    
    if (self.disclosureIndicatorView.image == nil) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
}


-(void)updateConstraintsDisclosureStyle
{
    [self removeConstraintsForView:self.disclosureIndicatorView onView:self.contentView];
    [self removeConstraintsForView:self.thumbnailView onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.disclosureIndicatorView != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.disclosureIndicatorView
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding-(self.disclosureIndicatorView.frame.size.width / 2.0)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.disclosureIndicatorView
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
    }
    
    if (self.disclosureIndicatorView.image != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.disclosureIndicatorView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:-self.rightPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                         multiplier:1
                                         constant:self.thumbnailView.image.size.width]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                         multiplier:1
                                         constant:self.thumbnailView.image.size.height]];
    }
    else {
        if (self.thumbnailView != nil) {
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.thumbnailView
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeRight
                                             multiplier:1
                                             constant:-self.rightPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.thumbnailView
                                             attribute:NSLayoutAttributeCenterY
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeCenterY
                                             multiplier:1
                                             constant:0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.thumbnailView
                                             attribute:NSLayoutAttributeWidth
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:nil
                                             attribute:NSLayoutAttributeNotAnAttribute
                                             multiplier:1
                                             constant:self.thumbnailView.image.size.width]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.thumbnailView
                                             attribute:NSLayoutAttributeHeight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:nil
                                             attribute:NSLayoutAttributeNotAnAttribute
                                             multiplier:1
                                             constant:self.thumbnailView.image.size.height]];
        }
    }
    
    if (self.titleLabel != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.thumbnailView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
}

- (void)layoutButtonStyle {

	self.accessoryType = UITableViewCellAccessoryNone;

	self.disclosureIndicatorView.hidden = YES;
	self.titleLabel.hidden = YES;
    
    if (self.image != nil) {
	
		self.removeImageButton.hidden = NO;
		self.addImageButton.hidden = YES;
        
    }
    else {
	
		self.removeImageButton.hidden = YES;
		self.addImageButton.hidden = NO;

	}

}

-(void)updateConstraintsButtonStyle
{
    [self removeConstraintsForView:self.thumbnailView onView:self.contentView];
    [self removeConstraintsForView:self.removeImageButton onView:self.contentView];
    [self removeConstraintsForView:self.addImageButton onView:self.contentView];
    
    if (self.thumbnailView != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:(self.thumbnailView.image.size.width/2)+self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                         multiplier:1
                                         constant:self.thumbnailView.image.size.width]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.thumbnailView
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                         multiplier:1
                                         constant:self.thumbnailView.image.size.height]];
    }
    
    if (self.removeImageButton != nil) {
    
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.removeImageButton
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
								
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.removeImageButton
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
								
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.removeImageButton
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.thumbnailView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:self.padding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.removeImageButton
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
    }
    if (self.addImageButton != nil) {
        

        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.addImageButton
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
								
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.addImageButton
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
								
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.addImageButton
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.padding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.addImageButton
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
    }
}


- (BOOL)select {
    
    [self.formViewController dismissAllKeyboards];
    
	TDImageSelectViewController *vc = [TDImageSelectViewController imageSelectForCell:self];
	[self.formViewController.navigationController pushViewController:vc animated:YES];
    

    return YES;
}

- (void)delete {
	
	self.image = nil;
	
	[self setNeedsDisplay];
	[self setNeedsLayout];
}

- (id)value {
    
    return self.image;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[UIImage class]]) {
        self.image = value;
    }
}


@end
