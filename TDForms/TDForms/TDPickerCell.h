//
//  TDPickerCell.h
//  CleverAccounts
//
//  Created by pw on 25/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"

@interface TDPickerCell : TDCell

+ (TDPickerCell*)pickerCellWithTitle:(NSString*)title values:(NSArray*)values key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UITextField *valueTextField;

@property (strong, nonatomic) NSArray *values;

@property (strong, nonatomic) UIPickerView *pickerView;

@property (strong, nonatomic) UIImageView *disclosureIndicatorView;

@property (assign, nonatomic) float titleToValuePadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float valueWidthAsProportion UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;

-(void) setPickerIndex: (int)index;

@end
