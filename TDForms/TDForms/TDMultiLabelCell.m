//
//  TDMultiLabelCell.m
//  CleverAccounts
//
//  Created by pw on 13/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDMultiLabelCell.h"

@interface TDMultiLabelCell()

@end

@implementation TDMultiLabelCell

+ (TDMultiLabelCell*)multiLabelCellWithKey:(NSString*)key
{
    TDMultiLabelCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.key = key;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.label1 = [[UILabel alloc] init];
        self.label1.translatesAutoresizingMaskIntoConstraints = NO;
        self.label1.backgroundColor = [UIColor clearColor];
        self.label1.numberOfLines = 1;
        self.label1.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.label1];
        
        self.label2 = [[UILabel alloc] init];
        self.label2.translatesAutoresizingMaskIntoConstraints = NO;
        self.label2.backgroundColor = [UIColor clearColor];
        self.label2.numberOfLines = 1;
        self.label2.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.label2];
        
        self.label3 = [[UILabel alloc] init];
        self.label3.translatesAutoresizingMaskIntoConstraints = NO;
        self.label3.backgroundColor = [UIColor clearColor];
        self.label3.numberOfLines = 1;
        self.label3.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.label3];
        
        self.label4 = [[UILabel alloc] init];
        self.label4.translatesAutoresizingMaskIntoConstraints = NO;
        self.label4.backgroundColor = [UIColor clearColor];
        self.label4.numberOfLines = 1;
        self.label4.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.label4];
        
        self.label5 = [[UILabel alloc] init];
        self.label5.translatesAutoresizingMaskIntoConstraints = NO;
        self.label5.backgroundColor = [UIColor clearColor];
        self.label5.numberOfLines = 1;
        self.label5.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.label5];
        
        self.label6 = [[UILabel alloc] init];
        self.label6.translatesAutoresizingMaskIntoConstraints = NO;
        self.label6.backgroundColor = [UIColor clearColor];
        self.label6.numberOfLines = 1;
        self.label6.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.label6];
        
    }
    return self;
}


- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.label1.textColor = self.textColor;
    self.label1.font = self.font;
    if (self.label1Color) self.label1.textColor = self.label1Color;
    if (self.label1Font) self.label1.font = self.label1Font;
    self.label1.textAlignment = NSTextAlignmentLeft;
    
    self.label2.textColor = self.textColor;
    self.label2.font = self.font;
    if (self.label2Color) self.label2.textColor = self.label2Color;
    if (self.label2Font) self.label2.font = self.label2Font;
    self.label2.textAlignment = NSTextAlignmentCenter;
    
    self.label3.textColor = self.textColor;
    self.label3.font = self.font;
    if (self.label3Color) self.label3.textColor = self.label3Color;
    if (self.label3Font) self.label3.font = self.label3Font;
    self.label3.textAlignment = NSTextAlignmentRight;
    
    self.label4.textColor = self.textColor;
    self.label4.font = self.font;
    if (self.label4Color) self.label4.textColor = self.label4Color;
    if (self.label4Font) self.label4.font = self.label4Font;
    self.label4.textAlignment = NSTextAlignmentLeft;
    
    self.label5.textColor = self.textColor;
    self.label5.font = self.font;
    if (self.label5Color) self.label5.textColor = self.label5Color;
    if (self.label5Font) self.label5.font = self.label5Font;
    self.label5.textAlignment = NSTextAlignmentCenter;
    
    self.label6.textColor = self.textColor;
    self.label6.font = self.font;
    if (self.label6Color) self.label6.textColor = self.label6Color;
    if (self.label6Font) self.label6.font = self.label6Font;
    self.label6.textAlignment = NSTextAlignmentRight;
}

-(void)updateConstraintsForLabel:(UILabel *) label
{
    if (label != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:label
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:label
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
    }
}

-(void)updateConstraintsForLabelA:(UILabel *) label
{
    if (label != nil) {
        [self updateConstraintsForLabel:label];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:label
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:label
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
    }
}

-(void)updateConstraintsForLabelB:(UILabel *) label
{
    if (label != nil) {
        [self updateConstraintsForLabel:label];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:label
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:label
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.label1 onView:self.contentView];
    [self removeConstraintsForView:self.label2 onView:self.contentView];
    [self removeConstraintsForView:self.label3 onView:self.contentView];
    [self removeConstraintsForView:self.label4 onView:self.contentView];
    [self removeConstraintsForView:self.label5 onView:self.contentView];
    [self removeConstraintsForView:self.label6 onView:self.contentView];
    
    if (self.label1 != nil) {
        [self updateConstraintsForLabelA:self.label1];
    }
    
    if (self.label2 != nil) {
        [self updateConstraintsForLabelA:self.label2];
    }
    
    if (self.label3 != nil) {
        [self updateConstraintsForLabelA:self.label3];
    }
    
    if (self.label4 != nil) {
        [self updateConstraintsForLabelB:self.label4];
    }
    
    if (self.label5 != nil) {
        [self updateConstraintsForLabelB:self.label5];
    }
    
    if (self.label6 != nil) {
        [self updateConstraintsForLabelB:self.label6];
    }
    
}

- (id)value {
    
    return nil;
}

@end
