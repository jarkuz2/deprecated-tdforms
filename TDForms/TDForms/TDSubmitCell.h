//
//  TDSubmitCell.h
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TDCell.h"

@class TDButton;

@interface TDSubmitCell : TDCell

+ (TDSubmitCell*)submitCellWithTitle:(NSString*)title;

@property (strong, nonatomic) NSString *buttonTitle UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) TDButton *button;

@end
