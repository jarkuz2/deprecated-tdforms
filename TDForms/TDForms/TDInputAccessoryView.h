//
//  TDInputAccessoryView.h
//  CleverAccounts
//
//  Created by pw on 11/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDCell.h"

@interface TDInputAccessoryView : UIToolbar

+ (TDInputAccessoryView*)inputAccessoryViewForCell:(TDCell*)cell;

@end
