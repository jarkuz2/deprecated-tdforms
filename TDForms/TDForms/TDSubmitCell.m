//
//  TDSubmitCell.m
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDSubmitCell.h"
#import "TDButton.h"
#import "TDFormViewController.h"

@implementation TDSubmitCell

+ (TDSubmitCell*)submitCellWithTitle:(NSString *)title
{
    TDSubmitCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    [cell.button setTitle:title forState:UIControlStateNormal];
    cell.buttonTitle = title;
    
    return cell;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.button = [TDButton buttonWithType:UIButtonTypeRoundedRect];
        [self.button addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
        self.button.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.button];

    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];

    if (self.backgroundImage != nil && self.button.buttonType == UIButtonTypeRoundedRect) {
        
        [self.button removeFromSuperview];
        
        self.button = [TDButton buttonWithType:UIButtonTypeCustom];
        [self.button setImage:self.backgroundImage forState:UIControlStateNormal];
        [self.button setTitle:self.buttonTitle forState:UIControlStateNormal];
        [self.button addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
        self.button.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.button];
        [self setNeedsUpdateConstraints];
    }
}

-(void)updateConstraints
{
    if (self.button != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.button
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.button
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.button
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.button
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    [super updateConstraints];
}

- (void)submit
{
    [self.formViewController submit];
}

@end
