//
//  TDInputAccessoryView.m
//  CleverAccounts
//
//  Created by pw on 11/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDInputAccessoryView.h"

@interface TDInputAccessoryView()

@property (weak, nonatomic) TDCell *cell;

@end

@implementation TDInputAccessoryView

+ (TDInputAccessoryView*)inputAccessoryViewForCell:(TDCell*)cell;
{
    TDInputAccessoryView *view = [[TDInputAccessoryView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 44)];
    
    view.cell = cell;

    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:view action:@selector(done)];
    UIBarButtonItem *nextBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:view action:@selector(next)];
    UIBarButtonItem *prevBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStylePlain target:view action:@selector(prev)];
    
    UIBarButtonItem *flexibleSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [view setItems:@[prevBarButton, nextBarButton, flexibleSpaceBarButtonItem, doneBarButton]];

    return view;
}

- (void)done {
    
    [self.cell done];
}

- (void)next {
    
    [self.cell next];
}

- (void)prev {
    
    [self.cell prev];
}

@end
