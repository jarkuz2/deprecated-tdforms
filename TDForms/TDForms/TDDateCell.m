//
//  TDDateCell.m
//  CleverAccounts
//
//  Created by pw on 25/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDDateCell.h"
#import <QuartzCore/QuartzCore.h>
#import "TDTextField.h"
#import "TDFormViewController.h"

@interface TDDateCell() <UIActionSheetDelegate>

@end

@implementation TDDateCell

+ (TDDateCell*)dateCellWithTitle:(NSString*)title value:(NSDate*)value key:(NSString*)key
{
    TDDateCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.value = value;
    cell.key = key;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _valueWidthAsProportion = 0.5;
        _titleToValuePadding = 0;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
    
        self.valueTextField = [[TDTextField alloc] init];
        self.valueTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueTextField.backgroundColor = [UIColor clearColor];
        self.valueTextField.textAlignment = NSTextAlignmentLeft;
        self.valueTextField.userInteractionEnabled = YES;
        self.valueTextField.inputAccessoryView = [TDInputAccessoryView inputAccessoryViewForCell:self];
        [self.contentView addSubview:self.valueTextField];
        

        self.datePicker = [[UIDatePicker alloc] init];
        
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        
        [self.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        
        if (self.value != nil) {
            self.datePicker.date = self.value;
        }
        
        self.valueTextField.inputView = self.datePicker;

    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;

    if (self.backgroundImage != nil) {
        self.valueTextField.background = self.backgroundImage;
    }
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.valueTextField onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.valueTextField != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeWidth
                                         multiplier:self.valueWidthAsProportion
                                         constant:1]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueTextField
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
    }
    
    if (self.titleLabel != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueTextField
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:-self.titleToValuePadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:self.topPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:-self.bottomPadding]];
    }
    
    [super updateConstraints];
}

- (BOOL)select {
    
    NSIndexPath *indexPath = [self.formViewController.tableView indexPathForRowAtPoint:self.center];
    
    [UIView animateWithDuration:0.3
                     animations: ^{
                         
                         [self.formViewController.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
                         
                     } completion: ^(BOOL finished){
                         
                         [self performSelector:@selector(doFirstResponder) withObject:nil afterDelay:0.1];
                     }
     ];
    
    return YES;
}

- (void)doFirstResponder {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.formViewController.scrolling = NO;
        [self.valueTextField becomeFirstResponder];
    });
}

- (void)done {
    
    [self.valueTextField resignFirstResponder];
}

- (void)dateChanged:(UIDatePicker*)sender {
    
    if (self.formatter == nil) {
        self.formatter = [[NSDateFormatter alloc] init];
        self.formatter.dateStyle = NSDateFormatterMediumStyle;
    }

	self.valueTextField.text = [self.formatter stringFromDate:sender.date];
    
    self.value = sender.date;
    
    [self.formViewController cellValueChanged:self];
}

- (id)value {
    
    return self.datePicker.date;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSDate class]]) {
        self.datePicker.date = value;
        
        if (self.formatter == nil) {
            self.formatter = [[NSDateFormatter alloc] init];
            self.formatter.dateStyle = NSDateFormatterMediumStyle;
        }
        
        self.valueTextField.text = [self.formatter stringFromDate:value];
    }
}

@end
