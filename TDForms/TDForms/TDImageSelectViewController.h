//
//  TDImageSelectViewController.h
//  CleverAccounts
//
//  Created by pw on 03/07/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDImageSelectCell.h"

@interface TDImageSelectViewController : UIViewController

+ (TDImageSelectViewController*)imageSelectForCell:(TDImageSelectCell*)parent;


@end
