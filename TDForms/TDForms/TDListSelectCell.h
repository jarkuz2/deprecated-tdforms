//
//  TDListSelectCell.h
//  CleverAccounts
//
//  Created by pw on 20/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"


@interface TDListSelectCell : TDCell

+ (TDListSelectCell*)listSelectCellWithTitle:(NSString*)title values:(NSArray*)items multipleSelect:(BOOL)multipleSelect key:(NSString*)key;

@property (assign) BOOL multipleSelect;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *valueLabel;
@property (strong, nonatomic) UIImageView *disclosureIndicatorView;
@property (strong, nonatomic) NSArray *items;

@property (assign, nonatomic) float titleToValuePadding UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float valueWidthAsProportion UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *titleColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *titleFont UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *valueColor UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIFont *valueFont UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *backButtonImage UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *selectImage UI_APPEARANCE_SELECTOR;

@end
