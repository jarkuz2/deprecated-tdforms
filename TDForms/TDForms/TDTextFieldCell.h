//
//  TDTextFieldCell.h
//  CleverAccounts
//
//  Created by pw on 24/05/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDCell.h"

@class TDTextField;

@interface TDTextFieldCell : TDCell

+ (TDTextFieldCell*)textFieldCellWithTitle:(NSString*)title placeholder:(NSString*)placeholder key:(NSString*)key;
+ (TDTextFieldCell*)textFieldCellWithTitle:(NSString*)title placeholder:(NSString*)placeholder value:(NSString*)value key:(NSString*)key;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) TDTextField *textField;

@property (assign, nonatomic) float textFieldWidthAsProportion UI_APPEARANCE_SELECTOR;
@property (assign, nonatomic) float titleToTextFieldPadding UI_APPEARANCE_SELECTOR;

@end
