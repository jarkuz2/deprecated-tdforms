//
//  TDButtonCell.h
//  CleverAccounts
//
//  Created by pw on 24/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDCell.h"

@interface TDButtonCell : TDCell

// titles and selectors must be the same length. images can be nil if you just want default bg image.
+ (TDButtonCell*)buttonCellWithLabelText:(NSString*)title titles:(NSArray*)titles images:(NSArray*)images selectorStrings:(NSArray*)selectors target:(id)target;
+ (TDButtonCell*)buttonCellWithTitles:(NSArray*)titles images:(NSArray*)images selectorStrings:(NSArray*)selectors target:(id)target;

@property (assign) float padding UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) NSString *labelText;
@property (strong, nonatomic) UILabel *titleLabel;

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray *selectors;
@property (nonatomic, strong) id targetForAllSelectors;

@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) NSMutableArray *buttonWidthConstraints;

@end
