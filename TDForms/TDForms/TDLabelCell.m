//
//  TDLabelCell.m
//  CleverAccounts
//
//  Created by pw on 13/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDLabelCell.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MapKit/MapKit.h>
#import "TDFormViewController.h"

@interface TDLabelCell() <MFMailComposeViewControllerDelegate, UIActionSheetDelegate>

@property (assign) float originalHeight;

@property (strong) UIButton *valueButton;

@property (strong) UIActionSheet *browserActionSheet;

@end

@implementation TDLabelCell


+ (TDLabelCell*)labelCellWithTitle:(NSString*)title subtitle:(NSString*)subtitle value:(NSString*)value key:(NSString*)key
{
    TDLabelCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.subtitleLabel.text = subtitle;
    cell.valueLabel.text = value;
    cell.key = key;
    
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleToSubtitlePadding = 2;
        _titleToValuePadding = 8;
        _valueWidthAsProportion = 0.4;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.titleLabel];
        
        self.subtitleLabel = [[UILabel alloc] init];
        self.subtitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.subtitleLabel.backgroundColor = [UIColor clearColor];
        self.subtitleLabel.numberOfLines = 0;
        self.subtitleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.subtitleLabel];
        
        self.valueLabelBackgroundImageView = [[UIImageView alloc] init];
        self.valueLabelBackgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.valueLabelBackgroundImageView];
        
        self.valueLabel = [[UILabel alloc] init];
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.valueLabel.backgroundColor = [UIColor clearColor];
        self.valueLabel.textAlignment = NSTextAlignmentRight;
        self.valueLabel.numberOfLines = 0;
        self.valueLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self.contentView addSubview:self.valueLabel];
        
        self.valueButton = [[UIButton alloc] init];
        self.valueButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.valueButton addTarget:self action:@selector(action) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.valueButton];
        
        self.originalHeight = self.cellHeight;
        
        [self.valueLabel addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionPrior context:NULL];
        
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    
    self.cellHeight = self.originalHeight;
    
    CGSize titleSize = CGSizeZero;
    if (self.titleLabel.frame.size.width > 0 && self.titleLabel.text != nil && self.titleLabel.adjustsFontSizeToFitWidth == NO)
        titleSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.titleLabel.frame.size.width,MAXFLOAT)];
    
    CGSize subtitleSize = CGSizeZero;
    if (self.subtitleLabel.frame.size.width > 0 && self.subtitleLabel.text != nil && self.subtitleLabel.adjustsFontSizeToFitWidth == NO)
        subtitleSize = [self.subtitleLabel.text sizeWithFont:self.subtitleLabel.font constrainedToSize:CGSizeMake(self.subtitleLabel.frame.size.width,MAXFLOAT)];
    
    CGSize valueSize = CGSizeZero;
    if (self.valueLabel.frame.size.width > 0 && self.valueLabel.text != nil && self.valueLabel.adjustsFontSizeToFitWidth == NO)
        valueSize = [self.valueLabel.text sizeWithFont:self.valueLabel.font constrainedToSize:CGSizeMake(self.valueLabel.frame.size.width,MAXFLOAT)];
    
    float minHeight = MAX(titleSize.height+subtitleSize.height+self.titleToSubtitlePadding, valueSize.height);
    minHeight = minHeight + self.topPadding + self.bottomPadding;
    
    self.cellHeight = minHeight;
    
    [self.formViewController refresh];
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;
    self.subtitleLabel.textColor = self.textColor;
    self.subtitleLabel.font = self.font;
    self.valueLabel.textColor = self.textColor;
    self.valueLabel.font = self.font;
    
    if (self.titleColor) self.titleLabel.textColor = self.titleColor;
    if (self.titleFont) self.titleLabel.font = self.titleFont;
    if (self.subtitleColor) self.subtitleLabel.textColor = self.subtitleColor;
    if (self.subtitleFont) self.subtitleLabel.font = self.subtitleFont;
    if (self.valueColor) self.valueLabel.textColor = self.valueColor;
    if (self.valueColorWhenActionable && self.actionType) self.valueLabel.textColor = self.valueColorWhenActionable;
    if (self.valueFont) self.valueLabel.font = self.valueFont;
    
    if (self.valueLabel.text == nil && self.subtitleLabel.text == nil && self.titleFontWhenNoSubtitleOrValue != nil)
        self.titleLabel.font = self.titleFontWhenNoSubtitleOrValue;
    
    if (self.valueLabel.text != nil && self.valueLabelBackgroundImage != nil) {
        
        self.valueLabelBackgroundImageView.image = self.valueLabelBackgroundImage;
    }
    
    
    if (self.titleLabel.text == nil && self.subtitleLabel.text == nil) {
        
        //only value, fill the whole width and left align the value text
        
        self.valueLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    else {
        if (self.subtitleLabel.text == nil) {
            
            if (self.valueLabel.text != nil) {
                
                [self.titleLabel sizeToFit];
            }
        }
    }
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    [self removeConstraintsForView:self.subtitleLabel onView:self.contentView];
    [self removeConstraintsForView:self.valueLabel onView:self.contentView];
    [self removeConstraintsForView:self.valueButton onView:self.contentView];
    [self removeConstraintsForView:self.valueLabelBackgroundImageView onView:self.contentView];
    
    if (self.titleLabel.text == nil && self.subtitleLabel.text == nil) {
        
        //only value, fill the whole width and left align the value text
        
        if (self.valueLabel != nil) {
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeLeft
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeLeft
                                             multiplier:1
                                             constant:self.leftPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeRight
                                             multiplier:1
                                             constant:-self.rightPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1
                                             constant:self.topPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1
                                             constant:-self.bottomPadding]];
            
        }
    }
    else {
        
        //normal value layout
        if (self.valueLabel != nil) {
            
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeWidth
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeWidth
                                             multiplier:self.valueWidthAsProportion
                                             constant:0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeRight
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeRight
                                             multiplier:1
                                             constant:-self.rightPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1
                                             constant:self.topPadding]];
            
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1
                                             constant:-self.bottomPadding]];
            
        }
        
        if (self.titleLabel != nil) {
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:self.titleLabel
                                             attribute:NSLayoutAttributeLeft
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeLeft
                                             multiplier:1
                                             constant:self.leftPadding]];
        }
        
        if (self.subtitleLabel.text == nil) {
            
            if (self.valueLabel.text == nil) {
                
                //only title, vertically align it in cell
                if (self.titleLabel != nil) {
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.contentView
                                                     attribute:NSLayoutAttributeRight
                                                     multiplier:1
                                                     constant:-self.rightPadding]];
                    
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.contentView
                                                     attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                     constant:self.topPadding]];
                    
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.contentView
                                                     attribute:NSLayoutAttributeBottom
                                                     multiplier:1
                                                     constant:-self.bottomPadding]];
                }
                
            }
            else {
                
                //title and value, vertically align title to top of value
                if (self.titleLabel != nil) {
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.valueLabel
                                                     attribute:NSLayoutAttributeLeft
                                                     multiplier:1
                                                     constant:-self.titleToValuePadding]];
                    
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.contentView
                                                     attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                     constant:self.topPadding]];
                }
            }
        }
        else {
            
            //title AND subtitle, vertically align centrally both of them together
            
            if (self.valueLabel.text == nil) {
                if (self.titleLabel != nil) {
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.contentView
                                                     attribute:NSLayoutAttributeRight
                                                     multiplier:1
                                                     constant:-self.rightPadding]];
                }
            }
            else {
                if (self.titleLabel != nil) {
                    [self.contentView addConstraint:[NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.valueLabel
                                                     attribute:NSLayoutAttributeLeft
                                                     multiplier:1
                                                     constant:-self.titleToValuePadding]];
                }
            }
            if (self.titleLabel != nil) {
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:self.titleLabel
                                                 attribute:NSLayoutAttributeTop
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contentView
                                                 attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                 constant:self.topPadding]];
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:self.titleLabel
                                                 attribute:NSLayoutAttributeBottom
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contentView
                                                 attribute:NSLayoutAttributeBottom
                                                 multiplier:0.5
                                                 constant:-(self.titleToSubtitlePadding/2.0)]];
            }
            //align subtitle below title
            
            if (self.subtitleLabel != nil) {
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:self.subtitleLabel
                                                 attribute:NSLayoutAttributeLeft
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contentView
                                                 attribute:NSLayoutAttributeLeft
                                                 multiplier:1
                                                 constant:self.leftPadding]];
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:self.subtitleLabel
                                                 attribute:NSLayoutAttributeRight
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.valueLabel
                                                 attribute:NSLayoutAttributeLeft
                                                 multiplier:1
                                                 constant:-self.titleToValuePadding]];
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:self.subtitleLabel
                                                 attribute:NSLayoutAttributeTop
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.titleLabel
                                                 attribute:NSLayoutAttributeBottom
                                                 multiplier:1
                                                 constant:self.titleToSubtitlePadding/2.0]];
                
                [self.contentView addConstraint:[NSLayoutConstraint
                                                 constraintWithItem:self.subtitleLabel
                                                 attribute:NSLayoutAttributeBottom
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contentView
                                                 attribute:NSLayoutAttributeBottom
                                                 multiplier:1
                                                 constant:-self.bottomPadding]];
            }
        }
    }
    
    if (self.valueLabelBackgroundImageView != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabelBackgroundImageView
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabelBackgroundImageView
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabelBackgroundImageView
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueLabelBackgroundImageView
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:0]];
    }
    
    if (self.valueButton != nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueButton
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueButton
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueButton
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeTop
                                         multiplier:1
                                         constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.valueButton
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.valueLabel
                                         attribute:NSLayoutAttributeBottom
                                         multiplier:1
                                         constant:0]];
    }
    [super updateConstraints];
}

- (void)action {
    
    if (self.actionType == TDLabelCellActionTelephone) {
        
        UIActionSheet *act = [[UIActionSheet alloc]initWithTitle:[NSString stringWithFormat:@"Call: %@",self.valueLabel.text] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call Contact", nil];
        act.tag = TDLabelCellActionTelephone;
        
        [act showInView:[UIApplication sharedApplication].keyWindow];
    }
    else if (self.actionType == TDLabelCellActionEmail) {
        
        UIActionSheet *act = [[UIActionSheet alloc]initWithTitle:@"Confirm Action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email Contact", nil];
        
        act.tag = TDLabelCellActionEmail;
        
        [act showInView:[UIApplication sharedApplication].keyWindow];
    }
    else if (self.actionType == TDLabelCellActionWeb) {
        
        NSString *edittedString;
        if ([self.valueLabel.text rangeOfString:@"http://"].location == NSNotFound) {
            
            edittedString = [NSString stringWithFormat:@"http://%@",self.valueLabel.text];
            
        } else {
            
            edittedString = self.valueLabel.text;
        }
        
        if ([[UIApplication sharedApplication] canOpenURL:[self chromeURLFromStandardURL:[NSURL URLWithString:edittedString]]]) {
            self.browserActionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select a web browser" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Safari",@"Chrome", nil];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:edittedString]];
            [self.browserActionSheet showInView:[UIApplication sharedApplication].keyWindow];
        }
        else {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:edittedString]];
        }
        
    }
    else if (self.actionType == TDLabelCellActionMap) {
        
        UIActionSheet *act = [[UIActionSheet alloc]initWithTitle:@"Confirm Action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"View in Maps App", nil];
        act.tag = TDLabelCellActionMap;
        
        [act showInView:[UIApplication sharedApplication].keyWindow];
    }
}

- (NSURL *)chromeURLFromStandardURL:(NSURL *)url
{
    NSString *scheme = url.scheme;
    
    // Replace the URL Scheme with the Chrome equivalent.
    NSString *chromeScheme = nil;
    if ([scheme isEqualToString:@"http"]) {
        chromeScheme = @"googlechrome";
    } else if ([scheme isEqualToString:@"https"]) {
        chromeScheme = @"googlechromes";
    }
    
    // Proceed only if a valid Google Chr ome URI Scheme is available.
    if (!chromeScheme) {
        return nil;
    }
    
    NSString *absoluteString = [url absoluteString];
    NSRange rangeForScheme = [absoluteString rangeOfString:@":"];
    NSString *urlNoScheme = [absoluteString substringFromIndex:rangeForScheme.location];
    NSString *chromeURLString = [chromeScheme stringByAppendingString:urlNoScheme];
    NSURL *chromeURL = [NSURL URLWithString:chromeURLString];
    
    return chromeURL;
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet == self.browserActionSheet) {
        
        if (buttonIndex == 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.valueLabel.text]];
        }
        else if (buttonIndex == 1) {
            [[UIApplication sharedApplication] openURL:[self chromeURLFromStandardURL:[NSURL URLWithString:self.valueLabel.text]]];
        }
    }
    else {
        switch (actionSheet.tag) {
            case TDLabelCellActionTelephone:
                
                if (buttonIndex == 1)
                {
                    break;
                }
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.valueLabel.text]]];
                
                break;
                
            case TDLabelCellActionEmail:
                
                if (buttonIndex == 1)
                {
                    break;
                } else {
                    
                    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                    controller.mailComposeDelegate = self;
                    [controller setSubject:@""];
                    [controller setMessageBody:@"" isHTML:NO];
                    
                    NSArray *theRecipient = @[self.theEmailAddress];
                    [controller setToRecipients:theRecipient];
                    
                    if (controller) [self.formViewController presentViewController:controller animated:YES completion:nil];
                }
                break;
            case TDLabelCellActionWeb:
                
                if (buttonIndex == 1)
                {
                    break;
                } else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.valueLabel.text]];
                }
                break;
            case TDLabelCellActionMap:
                
                if (buttonIndex == 1)
                {
                    break;
                } else {
                    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                    [geocoder geocodeAddressString:self.valueLabel.text
                                 completionHandler:^(NSArray *placemarks, NSError *error) {
                                     
                                     CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                                     MKPlacemark *placemark = [[MKPlacemark alloc]
                                                               initWithCoordinate:geocodedPlacemark.location.coordinate
                                                               addressDictionary:geocodedPlacemark.addressDictionary];
                                     
                                     MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                                     [mapItem setName:geocodedPlacemark.name];
                                     
                                     [MKMapItem openMapsWithItems:@[mapItem] launchOptions:@{}];
                                     
                                 }];
                }
                break;
            default:
                break;
        }
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    [self.formViewController dismissViewControllerAnimated:YES completion:nil];
}

- (id)value {
    
    return self.valueLabel.text;
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSString class]]) {
        self.valueLabel.text = value;
    }
    else if ([value isKindOfClass:[NSNumber class]]) {
        self.valueLabel.text = [value stringValue];
    }
}

- (void)dealloc {
    
    [self.valueLabel removeObserver:self forKeyPath:@"bounds"];
}



@end
