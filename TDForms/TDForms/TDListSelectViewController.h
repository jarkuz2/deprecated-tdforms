//
//  TDListSelectViewController.h
//  CleverAccounts
//
//  Created by pw on 03/07/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDListSelectCell.h"

@interface TDListSelectViewController : UITableViewController

+ (TDListSelectViewController*)listSelectForCell:(TDListSelectCell*)parent;


@end
