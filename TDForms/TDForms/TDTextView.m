//
//  TDTextView.m
//  CleverAccounts
//
//  Created by pw on 02/07/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDTextView.h"
#import "TDFormViewController.h"

@interface TDTextView()

@property (strong) UIImageView *bg;

@end

@implementation TDTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

        self.bg = [[UIImageView alloc] initWithFrame:self.bounds];
        
        [self insertSubview:self.bg atIndex:0];
    }
    return self;
}

- (BOOL)resignFirstResponder {
    
    if ([self.superview.superview.superview isKindOfClass:[UITableView class]]) {
        if ([((UITableView*)self.superview.superview.superview).dataSource isKindOfClass:[TDFormViewController class]]) {
            
            TDFormViewController *formVC = ((TDFormViewController*)((UITableView*)self.superview.superview.superview).dataSource);
            if (formVC.scrolling) {
                return NO;
            }
        }
    }
    return [super resignFirstResponder];
}


-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.bg.image = self.backgroundImage;
    self.bg.frame = self.bounds;
}


@end
