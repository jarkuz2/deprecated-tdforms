//
//  TDSwitchCell.m
//  CleverAccounts
//
//  Created by pw on 25/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDSwitchCell.h"
#import "TDFormViewController.h"

@interface TDSwitchCell()

@property (assign) BOOL onOff;

@end

@implementation TDSwitchCell

+ (TDSwitchCell*)switchCellWithTitle:(NSString*)title value:(NSNumber*)value key:(NSString*)key
{
    TDSwitchCell *cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self uuid]];
    
    cell.titleLabel.text = title;
    cell.key = key;
    cell.value = value;
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        
 
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if (self.switchControl == nil && self.buttonControl == nil) {
        
        
        //If the 'onImage' is set, this switch is now an checkbox type switch
        if (self.onImage != nil) {
        
            
            self.buttonControl = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.onImage.size.width, self.onImage.size.height)];
            [self.buttonControl addTarget:self action:@selector(selectedUnselected) forControlEvents:UIControlEventTouchUpInside];
            
            
            //TODO the library shouldnt reference resources - background image needs to be a property
            [self.buttonControl setBackgroundImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            self.buttonControl.translatesAutoresizingMaskIntoConstraints = NO;
            
            [self.contentView addSubview:self.buttonControl];
            
            [self setNeedsUpdateConstraints];
            
            if ([self.value boolValue] == YES) {
            
                [self.buttonControl setImage:self.onImage forState:UIControlStateNormal];
            }
            
            
        }
        else {
            //or a switch type of control
            
            self.switchControl = [[UISwitch alloc] init];
            [self.switchControl setOn:[self.value boolValue]];
            self.switchControl.translatesAutoresizingMaskIntoConstraints = NO;
            [self.switchControl addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
            
            [self.contentView addSubview:self.switchControl];
            [self setNeedsUpdateConstraints];
        }
    }
    
    self.titleLabel.textColor = self.textColor;
    self.titleLabel.font = self.font;

}

-(void)setOnImage:(UIImage *)onImage
{
    _onImage = onImage;
    
    NSLog(@"on");
}

-(void)updateConstraints
{
    [self removeConstraintsForView:self.switchControl onView:self.contentView];
    [self removeConstraintsForView:self.titleLabel onView:self.contentView];
    
    if (self.switchControl != nil) {
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.switchControl
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.switchControl
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
        
    }
    
    if (self.buttonControl !=nil) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.buttonControl
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.buttonControl
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:-self.rightPadding]];
        
    }
    
    if (self.titleLabel != nil && (self.buttonControl != nil || self.switchControl != nil)) {
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1
                                         constant:self.leftPadding]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.switchControl ? self.switchControl : self.buttonControl
                                         attribute:NSLayoutAttributeRight
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.titleLabel
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeHeight
                                         multiplier:1.0
                                         constant:0]];
    }
    
    [super updateConstraints];
}

-(void)selectedUnselected {
    
    if ([self.value boolValue] == YES) {

        self.value = @NO;
    }
    
    else {

        self.value = @YES;
    }

    [self.formViewController cellValueChanged:self];
}


-(void)switchChanged:(id)sender {
    
    [self.formViewController cellValueChanged:self];
}


- (id)value {
    
    return [NSNumber numberWithBool:self.onOff];
}

- (void)setValue:(id)value {
    
    if ([value isKindOfClass:[NSNumber class]]) {
        
        self.onOff = [value boolValue];
        
        if (self.switchControl != nil) {
            
            self.switchControl.on = [value boolValue];
        }
        else {
            
            if ([value boolValue] == YES) {
                
                [self.buttonControl setImage:nil forState:UIControlStateNormal];
            }
            
            else {
                
                [self.buttonControl setImage:self.onImage forState:UIControlStateNormal];
            }
        }
    }
    
    if ([self.value boolValue] == YES) {
        
        [self.buttonControl setImage:self.onImage forState:UIControlStateNormal];
    }
    
    else {
        
        [self.buttonControl setImage:nil forState:UIControlStateNormal];
    }
}

- (BOOL)select {

    [self.formViewController dismissAllKeyboards];
    return YES;
}

- (void)done {

}

@end
