//
//  TDValidation.m
//  CleverAccounts
//
//  Created by pw on 06/06/2013.
//  Copyright (c) 2013 The Distance. All rights reserved.
//

#import "TDValidation.h"

@implementation TDValidation

+ (TDValidation*)validationWithRegex:(NSString*)regex message:(NSString*)message
{
    TDValidation *validation = [[TDValidation alloc] init];
    
    validation.regex = regex;
    validation.message = message;
    
    return validation;
}

+ (TDValidation*)nonNullValidationWithmessage:(NSString*)message
{
    TDValidation *validation = [[TDValidation alloc] init];
    
    validation.regex = @"./";
    validation.message = message;
    
    return validation;
}

- (BOOL)validate:(NSString*)string
{
    if (self.regex != nil) {
        
        return [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", self.regex] evaluateWithObject:string];
    }
    else {
        
        return YES;
    }
}

@end
